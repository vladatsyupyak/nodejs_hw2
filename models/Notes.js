const mongoose = require('mongoose');

const noteSchema = mongoose.Schema(
    {
        userId: {
            type: String,
            required: true,
        },
        completed: {
            type: Boolean,
            required: true,
        },
        text: {
            type: String,
            required: true,
        },
    },
    { timestamps: true },
);
const Note = mongoose.model('Note', noteSchema);

module.exports = {
    Note,
};
