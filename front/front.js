let usernameInput = document.getElementById('username')
let passwordInput = document.getElementById('password')
let registerBtn = document.getElementById('form_btn')
let root = document.getElementById('root')

let loginUsernameInput = document.getElementById('loginUsername')
let loginPasswordInput = document.getElementById('loginPassword')
let loginBtn = document.getElementById('loginSubmit')

let createNoteSection = document.querySelector('.create_note_section')
let createNoteBtn = document.getElementById('create_note_btn')
let notesWrapper = document.querySelector('.notes_wrapper')

let linkToSignUp = document.getElementById('link_to_signup')
let sinInForm = document.getElementById('form-signin')
let signUpForm = document.getElementById('form-signup')


function createNotesWindow() {
    root.innerHTML = ` 
    <div class="wrapper">
        <button type="button"  onclick="showOrHideCreateNoteSection()" class="btn btn-primary">add note</button>
    </div>
</div>
`
}

function createNote(text, checked) {
    console.log(checked)
    let el = document.createElement('div')
    el.innerHTML = `<div class="note_wrapp">
           <p>${text}</p>
           <input type="checkbox" class="checkBtn" name="checkDone">
            <button type="button" class="delete_btn btn btn-dark">delete</button>

        </div>`
    el.querySelector('.checkBtn').checked = checked
    return el
}

function onCheckBtnClick(ev){
    let noteId= ev.target.parentElement.id
     fetch(`http://localhost:8080/api/notes/${noteId}`, {
        method: 'PATCH',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
        },
    })
        .then((response) => response.json())
        .catch((error) => {
            console.error('Error:', error);
        });

}

function showOrHideCreateNoteSection() {
    if (createNoteSection.classList.contains('hidden')) {
        createNoteSection.classList.remove('hidden')
    } else {
        createNoteSection.classList.add('hidden')
    }
}
function getUsersNotesByFetch(){
    return fetch('http://localhost:8080/api/notes/?offset=0&limit=100', {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
        },
    })
        .then((response) => response.json())
        .catch((error) => {
            console.error('Error:', error);
        });


}
function placeAllUserNotes(){
    notesWrapper.innerHTML = null
    getUsersNotesByFetch().then(data=> {
        let noteArray = data.notes.map(obj=>{
            let note = createNote(obj.text, obj.completed)
            note.firstElementChild.id = obj._id
            return note
        })
        noteArray.forEach((el)=>notesWrapper.prepend(el))
    })
}
function onCreateBtnClick() {
    let text = document.getElementById('create_note_textarea').value
    if (sessionStorage.getItem('jwt_token')) {
        fetch('http://localhost:8080/api/notes', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
            body: JSON.stringify({text: text}),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log('Success:', data);

            })
            .then(placeAllUserNotes)
            .catch((error) => {
                console.error('Error:', error);
            });
    }
    showOrHideCreateNoteSection()
    notesWrapper.classList.remove('hidden')


}
loginBtn.addEventListener('click', async (ev) => {
    ev.preventDefault()
    let username = loginUsernameInput.value
    let password = loginPasswordInput.value
    const data = {
        username: username,
        password: password,
    };
    fetch('http://localhost:8080/api/auth/login', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log('Success:', data.jwt_token);
            return data.jwt_token
        })
        .then((data) => sessionStorage.setItem('jwt_token', data))
        .catch((error) => {
            console.error('Error:', error);
        });

    createNotesWindow()
    notesWrapper.classList.remove('hidden')
    placeAllUserNotes()
})

registerBtn.addEventListener('click', (ev) => {
    ev.preventDefault()
    let username = usernameInput.value
    let password = passwordInput.value
    const data = {
        username: username,
        password: password,
    };

    fetch('http://localhost:8080/api/auth/register', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    signUpForm.classList.add('hidden')
    sinInForm.classList.remove('hidden')

})

linkToSignUp.addEventListener('click', ()=>{
    sinInForm.classList.add('hidden')
    signUpForm.classList.remove('hidden')

})
notesWrapper.addEventListener('click', (ev)=>{
    if(ev.target.classList.contains('delete_btn')) {
       let noteId= ev.target.parentElement.id
        fetch(`http://localhost:8080/api/notes/${noteId}`, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                'Authorization': 'Bearer ' + sessionStorage.getItem('jwt_token')
            },
        })
            .then((response) => response.json())
            .then((data) => {
                console.log('Success:', data);

            })
            .then(placeAllUserNotes)
            .catch((error) => {
                console.error('Error:', error);
            });
    }
    if(ev.target.classList.contains('checkBtn')) {
        onCheckBtnClick(ev)
    }
})

createNoteBtn.addEventListener('click', onCreateBtnClick)



