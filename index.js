const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');
const bp = require('body-parser');
const cors = require('cors')

mongoose.connect('mongodb+srv://vlada:Vladochka2002@cluster0.wt2skpc.mongodb.net/notes?retryWrites=true&w=majority');

const { usersRouter } = require('./scr/usersRouter');
const { notesRouter } = require('./scr/notesRouter');

app.use(express.json());
app.use(cors())
app.use(morgan('tiny'));


app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

app.use('/api/', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
    try {
        console.log('hi ');

        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res) {
    console.error(err);
    res.status(500).send({ message: 'Server error' });
}
