const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/Users');
require('dotenv').config();

const registerUser = async (req, res, next) => {
    const { username, password } = req.body;
    console.log(username)
    console.log(password)
    const user = new User({
        username,
        password: await bcrypt.hash(password, 10),
    });
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:63342');
    User.create(user)
        .then(() => res.status(200).json({ message: 'Success' }))
        .catch((err) => {
            next(err);
        });
};

const loginUser = async (req, res) => {
    const user = await User.findOne({ username: req.body.username });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
        const payload = { username: user.username, userId: user._id };
        const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
        return res.json({
            message: 'Success',
            jwt_token: jwtToken,
        });
    }
    return res.status(400).json({ message: 'Not authorized' });
};

function getTokenPayload(req) {
    const { authorization } = req.headers;
    const [, token] = authorization.split(' ');
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    return tokenPayload;
}

const getUserProfile = async (req, res) => {
    const tokenPayload = getTokenPayload(req);
    const user = await User.findOne({ username: tokenPayload.username });

    req.user = {
        userId: tokenPayload.userId,
        username: tokenPayload.username,
    };
    return res.status(200).send({
        user: {
            _id: user._id,
            username: user.username,
            createdAt: user.createdAt,
        },
    });
};
const deleteUserProfile = async (req, res) => {
    const tokenPayload = getTokenPayload(req);
    const user = await User.findOneAndDelete({ username: tokenPayload.username });
    return res.status(200).send({
        message: `success delete ${user}`,
    });
};
const changeUserPassword = async (req, res) => {
    const tokenPayload = getTokenPayload(req);
    const user = await User.findOne({ username: tokenPayload.username });
    console.log();
    if (user && await bcrypt.compare(String(req.body.oldPassword), String(user.password))) {
        await bcrypt.hash(req.body.newPassword, 10);
        await User.updateOne(
            { username: tokenPayload.username },
            { password: await bcrypt.hash(req.body.newPassword, 10) },
        );
        await user.save();
    } else {
        res.status(401).send('password are not the same');
    }
    res.send({ message: 'Success' });
};
module.exports = {
    registerUser,
    loginUser,
    getUserProfile,
    deleteUserProfile,
    changeUserPassword,
};
