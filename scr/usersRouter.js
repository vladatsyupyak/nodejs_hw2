const express = require('express');

const router = express.Router();
const {
    registerUser, loginUser, getUserProfile, deleteUserProfile, changeUserPassword,
} = require('./usersService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);
router.get('/users/me', authMiddleware, getUserProfile);
router.delete('/users/me', authMiddleware, deleteUserProfile);
router.patch('/users/me', authMiddleware, changeUserPassword);

module.exports = {
    usersRouter: router,
};
