const jwt = require('jsonwebtoken');
const { Note } = require('../models/Notes');

function getTokenPayload(req) {
    const { authorization } = req.headers;
    const [, token] = authorization.split(' ');
    const tokenPayload = jwt.verify(token, process.env.SECRET_KEY);
    return tokenPayload;
}

const createNote = (req, res, next) => {
    const { text } = req.body;
    const user = getTokenPayload(req);
    const note = new Note({
        userId: user.userId,
        text,
        completed: false,
    });
    Note.create(note)
        .then(() => res.json({ message: 'Success' }))
        .catch((err) => {
            next(err);
        });
};

const getNotes = async (req, res) => {
    const user = getTokenPayload(req);
    const { offset } = req.query;
    const { limit } = req.query;
    const notes = await Note.find({ userId: user.userId }).skip(offset).limit(limit);

    res.send({
        offset,
        limit,
        count: await Note.count(),
        notes,
    });
};

const getNoteById = async (req, res) => {
    const { id } = req.params;
    const note = await Note.findById(id);
    console.log(note);
    res.send({ note });
};

const updateById = async (req, res) => {
    const { id } = req.params;
    await Note.findByIdAndUpdate(id, { text: req.body.text });
    res.status(200).send({
        message: 'Success',
    });
};
const setChecked = async (req, res) => {
    const { id } = req.params;
    const note = await Note.findById(id);
    const { completed } = note;
    await Note.findByIdAndUpdate(id, { completed: !completed });
    res.status(200).send({
        message: 'Success',
    });
};
const deleteNote = async (req, res) => {
    const { id } = req.params;
    await Note.findByIdAndDelete(id);

    res.status(200).send({
        message: 'Success',
    });
};
module.exports = {
    createNote,
    getNotes,
    getNoteById,
    updateById,
    setChecked,
    deleteNote,
};
