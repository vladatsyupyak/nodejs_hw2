const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddleware');
const {
    createNote, getNotes, getNoteById, updateById, setChecked, deleteNote,
} = require('./notesService');

router.post('/', authMiddleware, createNote);
router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, getNoteById);
router.put('/:id', authMiddleware, updateById);
router.patch('/:id', authMiddleware, setChecked);
router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
    notesRouter: router,
};
